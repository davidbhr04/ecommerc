﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer.Dtos
{
    public class ProductEntriesDTO
    {
        public int Id { get; set; }
        public string Product { get; set; }
        public string Supplier { get; set; }
        public int Product_Entry { get; set; }
        public DateTime Entry_Date { get; set; }
    }
}
