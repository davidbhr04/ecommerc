﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer.Dtos
{
    public class StockEntriesDto
    {
        public int Id { get; set; }
        public int Product_Id { get; set; }
        public int Supplier_Id { get; set; }
        public int Product_Entry { get; set; }
        public DateTime Entry_Date { get; set; }
    }
}
