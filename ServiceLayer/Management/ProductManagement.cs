﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitiesLayer;
using DataLayer;
using EntitiesLayer.Dtos;

namespace ServiceLayer
{
    public class ProductManagement
    {
        private readonly ProductChanges _context;

        public ProductManagement()
        {
            _context = new ProductChanges();
        }

        public PRODUCTS Search(int Id)
        {
            return _context.Search(Id);
        }

        public void Create(PRODUCTS product)
        {
            _context.Create(product);
        }

        public List<ProductDto> Read()
        {
            return _context.Read();
        }

        public List<PRODUCTS> ShopRead()
        {
            return _context.ShopRead();
        }

        public void Update(PRODUCTS model)
        {
            _context.Update(model);
        }

        public void Delete(int Id)
        {
            _context.Delete(Id);
        }

        public List<ProductDto> FilterByName(string name) 
        {
            return _context.FilterByName(name);
        }
    }
}
