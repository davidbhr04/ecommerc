﻿using DataLayer.DBChanges;
using EntitiesLayer;
using EntitiesLayer.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayer.Management
{
    public class ProductEntriesManagement
    {
        private readonly ProductEntriesChanges _context;

        public ProductEntriesManagement()
        {
            _context = new ProductEntriesChanges();
        }

        public List<ProductEntriesDTO> Read()
        {
            return _context.GetProductEntries();
        }

        public List<ProductDto> ProductCatalog()
        {
            return _context.ProductCatalog();
        }

        public List<SupplierDto> SupplierCatalog()
        {
            return _context.GetSuppliers();
        }

        public void CreateEntry(PRODUCT_ENTRIES entry) 
        {
            _context.CreateEntry(entry);           
        }

        public void Delete(int id)
        {
            _context.Delete(id);
        }
    }
}
