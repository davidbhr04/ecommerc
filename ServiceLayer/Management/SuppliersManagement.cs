﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceLayer.Management; 

namespace ServiceLayer.Management
{
    public class SuppliersManagement
    {
        private readonly SuppliersChanges _context;

        public SuppliersManagement()
        {
            _context = new SuppliersChanges();
        }

        public SUPPLIERS Search(int Id)
        {
            return _context.Search(Id);
        }

        public void Create(SUPPLIERS product)
        {
            _context.Create(product);
        }

        public List<SUPPLIERS> Read()
        {
            return _context.Read();
        }

        public void Update(SUPPLIERS model)
        {
            _context.Update(model);
        }

        public void Delete(int Id)
        {
            _context.Delete(Id);
        }
    }
}
