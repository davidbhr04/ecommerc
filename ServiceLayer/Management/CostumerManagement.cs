﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceLayer.Management;

namespace ServiceLayer.Management
{
    public class CostumersManagement
    {
        private readonly CostumerChanges _context;

        public CostumersManagement()
        {
            _context = new CostumerChanges();
        }

        public COSTUMERS Search(int Id)
        {
            return _context.Search(Id);
        }

        public void Create(COSTUMERS costumer)
        {
            _context.Create(costumer);
        }

        public List<COSTUMERS> Read()
        {
            return _context.Read();
        }

        public void Update(COSTUMERS costuner)
        {
            _context.Update(costuner);
        }

        public void Delete(int Id)
        {
            _context.Delete(Id);
        }

        public List<FilterCostumerByString_Result> Filter(string field,string value)
        {
            return _context.Filter(field, value);
        }
    }
}
