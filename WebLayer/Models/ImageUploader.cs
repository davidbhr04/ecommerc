﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebLayer.Models
{
    public class ImageUploader
    {
        public HttpPostedFileBase file { get; set; }
        public int id { get; set; }
    }
}