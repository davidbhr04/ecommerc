﻿using EntitiesLayer;
using ServiceLayer.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace WebLayer.Controllers
{
    public class SuppliersController : Controller
    {
        // GET: Suppliers
        private SuppliersManagement crud = null;

        public SuppliersController()
        {
            crud = new SuppliersManagement();
        }

        // GET: Suppliers
        public ActionResult ShowSuppliers()
        {
            var suppliers = crud.Read().ToList();
            return View(suppliers);
        }

        // GET: Register for Products
        public ActionResult AddSuppliers()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddSuppliers(SUPPLIERS supplier)
        {
            crud.Create(supplier);
            return RedirectToAction("ShowSuppliers");
        }

        // GET: Products edition
        public ActionResult EditSupplier(int Id)
        {
            var supplier = crud.Search(Id);
            return View(supplier);
        }

        [HttpPost]
        public ActionResult EditSupplier(SUPPLIERS supplier)
        {
            crud.Update(supplier);
            return RedirectToAction("ShowSuppliers");
        }

        public ActionResult DeleteSupplier(int Id)
        {
            crud.Delete(Id);
            return RedirectToAction("ShowSuppliers");
        }
    }
}