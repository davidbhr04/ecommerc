﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ServiceLayer;
using ServiceLayer.Management;

namespace WebLayer.Controllers
{
    public class ShopController : Controller
    {
        private readonly ProductManagement crud = new ProductManagement();
        // GET: Shop
        public ActionResult Index()
        {
            var model = crud.ShopRead();
            return View(model);
        }
    }
}