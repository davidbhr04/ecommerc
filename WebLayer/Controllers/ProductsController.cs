﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ServiceLayer;
using EntitiesLayer;
using System.IO;
using System.Web.Hosting;
using System.Web.UI;
using EntitiesLayer.Dtos;
using WebLayer.Models;

namespace WebLayer.Controllers
{   
    public class ProductsController : Controller
    {
        
        private ProductManagement crud = null;

        public ProductsController() 
        {
            crud = new ProductManagement();
        }

        // GET: Products
        public ActionResult ShowProducts()
        {
            var products = crud.Read();
            return View(products);
        }

        // GET: Register for Products
        public ActionResult AddProducts()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult AddProducts(PRODUCTS product)
        {
            crud.Create(product);
            return RedirectToAction("ShowProducts");
        }

        // GET: Products edition
        public ActionResult EditProducts(int Id)
        {
            var product = crud.Search(Id);
            return View(product);
        }

        [HttpPost]
        public ActionResult EditProducts(PRODUCTS product)
        {
            crud.Update(product);
            return RedirectToAction("ShowProducts");
        }

        public ActionResult DeleteProduct(int Id)
        {
            crud.Delete(Id);
            return RedirectToAction("ShowProducts");
        }
        
        [HttpPost]
        public ActionResult FilterByName(string name) 
        {
            var productos = crud.FilterByName(name);
            return View(productos);
        }

        public ActionResult AddImage(ImageUploader values)
        {
            var products = crud.Read();
            var product = products.Where(p => p.Id == values.id).FirstOrDefault();
            if (values.file != null)
            {
                string pic = product.Name + ".jpg";
                string path = Path.Combine(
                                       Server.MapPath("~/Img"), pic);
                values.file.SaveAs(path);
            }
            return RedirectToAction("ShowProducts");
        }
    }
}