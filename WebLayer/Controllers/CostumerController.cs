﻿using EntitiesLayer;
using ServiceLayer.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace WebLayer.Controllers
{
    public class CostumerController : Controller
    {
        // GET: Suppliers
        private CostumersManagement crud = null;

        public CostumerController()
        {
            crud = new CostumersManagement();
        }

        // GET: Suppliers
        public ActionResult ShowCostumers()
        {
            var costumer = crud.Read().ToList();
            return View(costumer);
        }

        // GET: Register for Products
        public ActionResult AddCostumers()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddCostumers(COSTUMERS costumer)
        {
            crud.Create(costumer);
            return RedirectToAction("ShowCostumers");
        }

        // GET: Products edition
        public ActionResult EditCostumers(int Id)
        {
            var costumer = crud.Search(Id);
            return View(costumer);
        }

        [HttpPost]
        public ActionResult EditCostumers(COSTUMERS costumer)
        {
            crud.Update(costumer);
            return RedirectToAction("ShowCostumers");
        }

        public ActionResult DeleteCostumers(int Id)
        {
            crud.Delete(Id);
            return RedirectToAction("ShowCostumers");
        }

        public ActionResult Filter((string field, string value) vals)
        {
            var costumers = crud.Filter(vals.field,vals.value);
            if (costumers.Count > 0)
            {
                return View(costumers);
            }
            else
            {
                return RedirectToAction("ShowCostumers");
            }
        }
    }
}