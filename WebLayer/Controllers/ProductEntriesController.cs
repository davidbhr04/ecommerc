﻿using EntitiesLayer;
using EntitiesLayer.Dtos;
using ServiceLayer;
using ServiceLayer.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebLayer.Controllers
{
    public class ProductEntriesController : Controller
    {
        // GET: ProductEntries
        private ProductEntriesManagement crud = null;

        public ProductEntriesController()
        {
            crud = new ProductEntriesManagement();
        }

        public ActionResult ShowEntries()
        {
            var products = crud.Read();
            return View(products);
        }

        // GET: ProductEntries Add
        public ActionResult AddStock()
        {
            List<ProductDto> products = crud.ProductCatalog();
            List<SelectListItem> ProductsCatalog = products.ConvertAll(d =>
            {
                return new SelectListItem()
                {
                    Text = d.Name.ToString(),
                    Value = d.Id.ToString(),
                    Selected = false
                };
            });

            List<SupplierDto> suppliers = crud.SupplierCatalog();
            List<SelectListItem> SuppliersCatalog = suppliers.ConvertAll(d =>
            {
                return new SelectListItem()
                {
                    Text = d.Name.ToString(),
                    Value = d.Id.ToString(),
                    Selected = false
                };
            });

            ViewBag.ProductsCatalog = ProductsCatalog;
            ViewBag.SuppliersCatalog = SuppliersCatalog;
            return View();
        }

        [HttpPost]
        public ActionResult AddStock(PRODUCT_ENTRIES entry)
        {
            crud.CreateEntry(entry);
            return RedirectToAction("ShowEntries");
        }

        public ActionResult DeleteEntry(int id)
        {
            crud.Delete(id);
            return RedirectToAction("ShowEntries");
        }
    }
}