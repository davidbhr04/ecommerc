﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitiesLayer;
using DataLayer;
using EntitiesLayer.Dtos;
using DataLayer.DBChanges;

namespace ServiceLayer
{
    public class ProductChanges 
    {
        public List<ProductDto> FilterByName(string name)
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var products = db.STOCK.ToList();
                var filter = ProductEntriesChanges.ProductsMap(products);
                return filter.Where(p => p.Name.Contains(name)).ToList();
            }

        }
        public PRODUCTS Search(int Id)
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var product = db.PRODUCTS.Find(Id);
                return product;
            }
        }

        public void Create(PRODUCTS product)
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                db.PRODUCTS.Add(product);
                db.SaveChanges();
            }
        }

        public List<ProductDto> Read()  
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var products = db.STOCK.ToList();
                return ProductEntriesChanges.ProductsMap(products);
            }
        }
        public List<PRODUCTS> ShopRead()
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var products = db.PRODUCTS.ToList();
                return products;
            }
        }
        public void Update(PRODUCTS model)
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var product = db.PRODUCTS.Find(model.Id);
                product.Name = model.Name;
                product.Price = model.Price;
                db.Entry(product).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void Delete(int Id)
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var product = db.PRODUCTS.Find(Id);
                var stock = db.STOCK.Where(s=> s.Product_Id == product.Id);
                db.STOCK.Remove(stock.FirstOrDefault());
                db.PRODUCTS.Remove(product);
                db.SaveChanges();
            }
        }
    }
}