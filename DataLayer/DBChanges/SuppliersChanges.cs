﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitiesLayer;
using DataLayer;

namespace ServiceLayer
{
    public class SuppliersChanges
    {
        public SUPPLIERS Search(int Id)
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var supplier = db.SUPPLIERS.Find(Id);
                return supplier;
            }
        }

        public void Create(SUPPLIERS supplier)
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                db.SUPPLIERS.Add(supplier);
                db.SaveChanges();
            }
        }

        public List<SUPPLIERS> Read()
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                return db.SUPPLIERS.ToList<SUPPLIERS>();
            }
        }

        public void Update(SUPPLIERS model)
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var supplier = db.SUPPLIERS.Find(model.Id);
                supplier.RNC = model.RNC;
                supplier.Name = model.Name;
                supplier.Phone = model.Phone;
                supplier.Email = model.Email;
                db.SaveChanges();
            }
        }

        public void Delete(int Id) 
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var supplier = db.SUPPLIERS.Find(Id);
                db.SUPPLIERS.Remove(supplier);
                db.SaveChanges();
            }
        }
    }
}