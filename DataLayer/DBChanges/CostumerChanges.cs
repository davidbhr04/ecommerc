﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitiesLayer;
using DataLayer;


namespace ServiceLayer
{
    public class CostumerChanges
    {
        public COSTUMERS Search(int Id)
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var costumer = db.COSTUMERS.Find(Id);
                return costumer;
            }
        }

        public void Create(COSTUMERS costumer)
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                db.COSTUMERS.Add(costumer);
                db.SaveChanges();
            }
        }

        public List<COSTUMERS> Read()
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                return db.COSTUMERS.ToList();
            }
        }

        public void Update(COSTUMERS model)
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var costumer = db.COSTUMERS.Find(model.Id);
                costumer.RNC = model.RNC;
                costumer.Name = model.Name;
                costumer.Phone = model.Phone;
                costumer.Email = model.Email;
                costumer.Categoty = model.Categoty;
                db.SaveChanges();
            }
        }

        public void Delete(int Id)
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var costumer = db.COSTUMERS.Find(Id);
                db.COSTUMERS.Remove(costumer);
                db.SaveChanges();
            }
        }

        public List<FilterCostumerByString_Result> Filter(string field,string value)
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var costumers =  db.FilterCostumerByString(field, value).ToList();
                return costumers;
            }
        }
            
    }
}