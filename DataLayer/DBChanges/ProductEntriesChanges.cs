﻿using EntitiesLayer;
using EntitiesLayer.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.DBChanges
{
    public class ProductEntriesChanges
    {

        public void AddToStock(StockEntriesDto dto)
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var entry = db.PRODUCT_ENTRIES.Find(dto);

                db.SaveChanges();
            }
        }

        private List<ProductDto> ProductCatalogGenerator(List<PRODUCTS> products)
        {
            List<ProductDto> Catalog = new List<ProductDto>();
            foreach (var item in products)
            {
                var product = new ProductDto();
                product.Id = item.Id;
                product.Name = item.Name;
                product.Price = item.Price;
                Catalog.Add(product);
            }
            return Catalog;
        }

        public List<ProductDto> ProductCatalog()
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var products = db.PRODUCTS.ToList();
                var catalog = ProductCatalogGenerator(products);
                return catalog;
            }
        }

        public List<SupplierDto> GetSuppliers()
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var suppliers = db.SUPPLIERS.ToList();
                var catalog = SuppliersCatalogGenerator(suppliers);
                return catalog;
            }
        }

        private List<SupplierDto> SuppliersCatalogGenerator(List<SUPPLIERS> suppliers)
        {
            List<SupplierDto> Catalog = new List<SupplierDto>();
            foreach (var item in suppliers)
            {
                var supplier = new SupplierDto();
                supplier.Id = item.Id;
                supplier.Name = item.Name;
                Catalog.Add(supplier);
            }
            return Catalog;
        }
        public static List<ProductDto> ProductsMap(List<STOCK> products)
        {
            List<ProductDto> Catalog = new List<ProductDto>();
            foreach (var item in products)
            {
                var product = new ProductDto();
                product.Id = item.PRODUCTS.Id;
                product.Name = item.PRODUCTS.Name;
                product.Price = item.PRODUCTS.Price;
                product.Quantity = item.Quantity;
                Catalog.Add(product);
            }
            return Catalog;
        }

        public void CreateEntry(PRODUCT_ENTRIES entry)
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                entry.Entry_Date = DateTime.Now;
                db.PRODUCT_ENTRIES.Add(entry);
                db.SaveChanges();
            }
        }

        public List<ProductEntriesDTO> GetProductEntries() 
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                List<ProductEntriesDTO> Catalog = new List<ProductEntriesDTO>();
                var p_entries = db.PRODUCT_ENTRIES.ToList();
                var products = db.STOCK.ToList();
                var suppliers = db.SUPPLIERS.ToList();
                foreach (var item in p_entries)
                {
                    var entry = new ProductEntriesDTO();
                    entry.Id = item.Id;
                    var supplier = suppliers.Where(s => s.Id == item.Supplier_Id).ToList();
                    entry.Product = products[0].PRODUCTS.Name;
                    entry.Supplier = supplier[0].Name;
                    entry.Product_Entry = item.Product_Entry;
                    entry.Entry_Date = item.Entry_Date;
                    Catalog.Add(entry);
                    
                }
                return Catalog;
            }
        }

        public void Delete(int id) 
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                var entry = db.PRODUCT_ENTRIES.Find(id);
                db.PRODUCT_ENTRIES.Remove(entry);
                db.SaveChanges();
            }
        }
    }
}
