﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class Filtering
    {
        private static string Capitalize(string value)
        {
            TextInfo txt = new CultureInfo("en-US", false).TextInfo;
            string _value = txt.ToTitleCase(value);
            return _value;
        }

        public static (List<COSTUMERS>,string) FilterCustomers(string value)
        {
            using (E_COMMERCEEntities db = new E_COMMERCEEntities())
            {
                string _value = Capitalize(value);
                string filteredBy = "";
                var costumers = db.COSTUMERS.ToList();
                List<COSTUMERS> lst = new List<COSTUMERS>();

                if (_value == "Premium" || _value == "Regular")
                {
                    lst = costumers.Where(c => c.Categoty.Contains(value)).ToList();
                    filteredBy = "Category";
                }
                else 
                {
                    lst = costumers.Where(c => c.Name.Contains(_value)).ToList();
                    filteredBy = "Name";
                }

                return (lst,filteredBy);
            }
        }
    }
}
